﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WMProductDataAccess.Areas;

namespace WMProduct.Areas.Product
{
    public class ProductController : Controller
    {
        private readonly IProductManager productManager;

        public ProductController(IProductManager productManager)
        {
            this.productManager = productManager;
        }
        public IActionResult Index()
        {
            var model = productManager.GetAll();
            return View(model);
        }

        [HttpPost]
        public IActionResult Update(ProductViewModel model)
        {
            var product = new WMProductDataAccess.Domain.Product
            {
                Id = model.Id,
                Name = model.Name,
                Manufacturer = model.Manufacturer,
                Supplier = model.Supplier,
                Price = model.Price,
                Category = model.Category,
                Description = model.Description
            };

            productManager.Update(product);

            return RedirectToAction("Index");

        }
        public IActionResult Update(int id)
        {
            var model = new ProductViewModel();

            if (id > 0)
            {
                var product = productManager.GetById(id);

                if (product != null)
                {
                    model.Id = product.Id;
                    model.Name = product.Name;
                    model.Manufacturer = product.Manufacturer;
                    model.Supplier = product.Supplier;
                    model.Price = product.Price;
                    model.Category = product.Category;
                    model.Description = product.Description;
                }
            }
            var products = productManager.GetAll();

            return View("UpdateProduct", model);
        }

        public ActionResult ProductReader()
        {
            var webClient = new WebClient();
            string json = webClient.DownloadString(@"C:\Users\Luka\Documents\WMproduct\WMProduct\wwwroot\lib\lista.json");
            List<ProductViewModel> products = JsonConvert.DeserializeObject<List<ProductViewModel>>(json);

            return PartialView("_ProductReaderPartial", products);
        }

        public ActionResult AddProductToJson()
        {
            return PartialView("_AddProductToJsonPartial");
        }

        public ActionResult AddToJson(ProductViewModel model)
        {

            var webClient = new WebClient();
            string json = webClient.DownloadString(@"C:\Users\Luka\Documents\WMproduct\WMProduct\wwwroot\lib\lista.json");
            List<ProductViewModel> products = JsonConvert.DeserializeObject<List<ProductViewModel>>(json);
            var existingInList = false;
            foreach (ProductViewModel prod in products.ToList())
            {
                if (prod.Name == model.Name)
                {
                    prod.Name = model.Name;
                    prod.Description = model.Description;
                    prod.Supplier = model.Supplier;
                    prod.Price = model.Price;
                    prod.Category = model.Category;
                    prod.Manufacturer = model.Manufacturer;
                    existingInList = true;
                    break;
                }
            }
            if (!existingInList)
            {
                var product = new ProductViewModel
                {
                    Id = model.Id,
                    Name = model.Name,
                    Manufacturer = model.Manufacturer,
                    Supplier = model.Supplier,
                    Price = model.Price,
                    Category = model.Category,
                    Description = model.Description
                };
                products.Add(product);
            }

            var convertedJson = JsonConvert.SerializeObject(products, Formatting.Indented);
            System.IO.File.WriteAllText(@"C:\Users\Luka\Documents\WMproduct\WMProduct\wwwroot\lib\lista.json", convertedJson);

            return RedirectToAction("Index");
        }
    }
}
