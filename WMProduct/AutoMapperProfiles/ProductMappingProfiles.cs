﻿using AutoMapper;
using WMProduct.Areas.Product;
using Entities = WMProductDataAccess.ApplicationDataService.EF.Entities;
using Domain = WMProductDataAccess.Domain;

namespace WMProduct.AutoMapperProfiles
{
    public class ProductMappingProfiles : Profile
    {
        public ProductMappingProfiles()
        {
            // Domain <-> Entity
            CreateMap<Domain.Product, Entities.Product>();
            CreateMap<Entities.Product, Domain.Product>();

            // Domain <-> ViewModel
            CreateMap<ProductViewModel, Domain.Product>();
            CreateMap<Domain.Product, ProductViewModel>();
        }
    }
}
