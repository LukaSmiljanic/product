﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WMProductDataAccess.ApplicationDataService.EF.Entities;

namespace WMProductDataAccess.ApplicationDataService
{
    public class WMProductDbContext : DbContext
    {
        public WMProductDbContext(DbContextOptions<WMProductDbContext> options) : base(options)
        {
        }
        public DbSet<Product> Products { get; set; }
    }

}

