﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WMProductDataAccess.App.Services.ApplicationDataService.EF;
using WMProductDataAccess.ApplicationDataService.EF.Entities;

namespace WMProductDataAccess.ApplicationDataService.EF
{
    public class EFProductData : BaseApplicationData, IProductData
    {
        public EFProductData(IMapper mapper, WMProductDbContext dbContext) : base(dbContext,mapper)
        {

        }
        public Domain.Product GetById(int id)
        {
            return mapper.Map<Domain.Product>(
                ctx.Products.AsNoTracking()
                .FirstOrDefault(s => s.Id == id));
        }

        public IEnumerable<Domain.Product> GetAll()
        {
            return ctx.Products.AsNoTracking()
                .Select(p => mapper.Map<Domain.Product>(p))
                .ToList();
            
        }

        public bool Update(Domain.Product product)
        {
            var productEntity = mapper.Map<Entities.Product>(product);

            using (var tx = ctx.Database.BeginTransaction())
            {
                if (productEntity.Id > 0)
                {
                    ctx.Products.Attach(productEntity);
                    ctx.Entry(productEntity).State = EntityState.Modified;
                }
                else
                {
                    ctx.Products.Add(productEntity);
                }

                ctx.SaveChanges();
                tx.Commit();
            }

            return true;
        }

    }
}
