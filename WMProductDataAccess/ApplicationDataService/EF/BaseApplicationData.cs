﻿using System.Collections.Generic;
using System.Linq;
using System;
using WMProductDataAccess.ApplicationDataService;
using AutoMapper;

namespace WMProductDataAccess.App.Services.ApplicationDataService.EF
{
    public class BaseApplicationData
    {
        protected WMProductDbContext ctx;
        protected IMapper mapper;

        public BaseApplicationData(WMProductDbContext context, IMapper mapper)
        {
            this.ctx = context;
            this.mapper = mapper;
        }
    }

}