﻿using System;
using System.Collections.Generic;
using System.Text;
using Twilio.Rest;
using WMProductDataAccess.ApplicationDataService.EF.Entities;

namespace WMProductDataAccess.ApplicationDataService
{
    public interface IProductData
    {
        Domain.Product GetById(int id);
        IEnumerable<Domain.Product> GetAll();
        bool Update(Domain.Product product);
    }
}
