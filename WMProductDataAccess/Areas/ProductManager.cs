﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WMProductDataAccess.ApplicationDataService;

namespace WMProductDataAccess.Areas
{
    public class ProductManager : IProductManager
    {
        protected readonly IMapper mapper;
        private readonly IProductData productData;
        public ProductManager(IProductData productData)
        {
            this.productData = productData;
        }

        public Domain.Product GetById(int id)
        {
            var product = productData.GetById(id);
            return product;
        }

        public IEnumerable<Domain.Product> GetAll()
        {
            var products = productData.GetAll();
            return products;
        }

        public bool Update(Domain.Product product)
        {
            return productData.Update(product);
        }
    }
}

