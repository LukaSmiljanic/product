﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WMProductDataAccess.Areas
{
    public interface IProductManager
    {
        Domain.Product GetById(int id);
        IEnumerable<Domain.Product> GetAll();
        bool Update(Domain.Product product);
    }
}
